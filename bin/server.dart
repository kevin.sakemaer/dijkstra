// Copyright (c) 2016, Kevin Segaud. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import "dart:io";
import "dart:async";
import "dart:convert";
import "dart:math" as math;

import "package:dijkstra/dijkstra.dart";

double degreeToRadians(double angle) {
  return (angle * math.PI) / 180.0;
}

double haversine(double angle) {
  return (math.sin(0.5 * (angle)));
}

double distanceBetweenNode(Node n1, Node n2) {
  double lat1 = degreeToRadians(n1.latitude);
  double lng1 = degreeToRadians(n1.longitude);
  double lat2 = degreeToRadians(n2.latitude);
  double lng2 = degreeToRadians(n2.longitude);
  double earth_radius = 6371008.8;
  double hav1 = haversine(lat2 - lat1);
  double hav2 = haversine(lng2 - lng1);
  double s1 = hav1 * hav1;
  double s2 = hav2 * hav2;
  double inner_sqrt = s1 + math.cos(lat1) * math.cos(lat2) * s2;
  return 2.0 * earth_radius * math.asin(math.sqrt(inner_sqrt));
}

class Road {
  double maxSpeed;
  bool oneWay;
  List<Node> nodes;

  Road(double this.maxSpeed, bool this.oneWay) {
    nodes = new List<Node>();
  }

  @override
  String toString() {
    StringBuffer sb = new StringBuffer();
    sb.writeln("$maxSpeed $oneWay");
    nodes.forEach((Node node) {
      sb.writeln("->  $node");
    });
    return sb.toString();
  }
}

Future<Set<Node>> csvParser(String file_name) async {
  File f = new File(file_name);
  if (!f.existsSync()) {
    return null;
  }
  Stream<String> lines =
      f.openRead().transform(UTF8.decoder).transform(new LineSplitter());
  Stream<List<double>> streamFields =
      lines.map((String line) => line.split(',')).map((List<String> sFields) {
    List<double> fields = <double>[];
    sFields.forEach((String field) {
      fields.add(double.parse(field, (String e) => -1.0));
    });
    return fields;
  });
  List<Road> roads = <Road>[];
  Set<Node> nodes = new Set();
  Map<String, Node> map_nodes = <String, Node>{};
  Map<Node, int> numberOccurenceNode = <Node, int>{};
  await for (List<double> fields in streamFields) {
    num numPoint = fields[5];
    Road road = new Road(fields[2], fields[4] == 1);
    for (int i = 0; i < numPoint; ++i) {
      Node node = new Node(fields[2 * i + 6], fields[2 * i + 7]);
      if (!map_nodes.containsKey(node.toString())) {
        map_nodes[node.toString()] = node;
      } else {
        node = map_nodes[node.toString()];
      }
      if (numberOccurenceNode[node] == null) {
        numberOccurenceNode[node] = 1;
      } else {
        if (++numberOccurenceNode[node] > 1) {
          nodes.add(node);
        }
      }
      road.nodes.add(node);
    }
    if (road.nodes.isNotEmpty) {
      nodes.add(road.nodes.first);
      nodes.add(road.nodes.last);
    }
    roads.add(road);
  }
  print(nodes.length);

  List<double> arc_durations = <double>[];
  for (Road road in roads) {
    int i = 0;
    while (i < road.nodes.length - 1) {
      int j = i + 1;
      while (!nodes.contains(road.nodes[j])) {
        j++;
      }
      double distance = 0.0;
      for (int k = i; k < j; ++k) {
        distance += distanceBetweenNode(road.nodes[k], road.nodes[k + 1]);
      }
      double duration = distance * 3.6 / road.maxSpeed;
      Node n1 = nodes.lookup(road.nodes[i]);
      Node n2 = nodes.lookup(road.nodes[j]);
      if (!road.oneWay) {
        arc_durations.add(duration);
        n2.addOutcomingArc(new Arc(n2, n1, duration));
        n2.addIncomingArc(new Arc(n1, n2, duration));
      }
      n1.addOutcomingArc(new Arc(n1, n2, duration));
      n1.addIncomingArc(new Arc(n2, n1, duration));
      arc_durations.add(duration);
      i = j;
    }
  }
  print(arc_durations.length);
  double sum = 0.0;
  arc_durations.forEach((double duration) {
    sum += duration;
  });
  print(sum);
  return nodes;
}

Future<int> main(List<String> args) async {
  if (args.length < 1) {
    print("No file in params");
    return 0;
  }
  Stopwatch sw = new Stopwatch();
  sw.start();

  List<Node> nodes = (await csvParser(args[0])).toList();
  if (nodes == null) {
    print("File ${args[0]} doen't exist");
    return 0;
  }

  //  ajoute le mode interaction
  Stream<String> lines =
      stdin.transform(UTF8.decoder).transform(new LineSplitter());

  await for (String line in lines) {
    if (line == "exit") {
      exit(1);
    }
    RegExp reg = new RegExp(
        r'([-|+]?\d+.\d+),([-|+]?\d+.\d+)->([-|+]?\d+.\d+),([-|+]?\d+.\d+)');
    Match matche = reg.firstMatch(line);
    if (matche == null || matche.groupCount != 4) {
      print("INVALID");
      continue;
    }
    double lat1 = double.parse(matche.group(1));
    double lng1 = double.parse(matche.group(2));
    double lat2 = double.parse(matche.group(3));
    double lng2 = double.parse(matche.group(4));

    //  data.csv
    int posNode1 = nodes.indexOf(new Node(lat1, lng1));
    int posNode2 = nodes.indexOf(new Node(lat2, lng2));

    //  test.csv
    // int posNode1 = nodes.indexOf(new Node(47.9840, 2.03));
    // int posNode2 = nodes.indexOf(new Node(47.9960, 2.0195));

    if (posNode1 == -1 || posNode2 == -1) {
      print("INVALID");
      continue;
    }

    Node start = nodes[posNode1];
    Node end = nodes[posNode2];

    DijkstraResult result = dijkstraSearch(nodes, start, end);
    // DijkstraResult result = d.bidirectionalSearch(nodes, start, end);
    if (result == null) return 0;
    List<double> durations =
        getArcDurationForEachStepFromDijkstra(result, start, end);
    if (durations == null) {
      print("NO PATH");
      return 0;
    }
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < durations.length; ++i) {
      double duration = durations[i];
      sb.write(duration);
      if (i != durations.length - 1) sb.write(" ");
    }
    print("${result.dist[end]} ${sb.toString()}");
    // print("my result            : ${result.dist[end]}");
    // print("path                 : ${sb.toString()}");
    // print("result expected      : 1363.781432");

  }

  sw.stop();
  // print(sw.elapsed);

  return 1;
}
