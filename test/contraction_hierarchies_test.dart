// Copyright (c) 2016, Kevin Segaud. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

// import 'package:contraction_hierarchies/contraction_hierarchies.dart' as ch;
// import 'package:test/test.dart';

void main() {
  // group("utils", () {
  //   test('degree to radians', () {
  //     expect(1.57089663267948966, deg)
  //   });
  // });

  // List<ch.Node> nodes;
  // ch.ContractionHierarchies c_h;
  //
  // group("basic nodes list", () {
  //   setUp(() {
  //     nodes = <ch.Node>[];
  //     ch.Node node_A = new ch.Node("A");
  //     ch.Node node_B = new ch.Node("B");
  //     ch.Node node_C = new ch.Node("C");
  //     ch.Node node_D = new ch.Node("D");
  //
  //     node_A.edges.add(new ch.Edge(1, node_B));
  //     node_A.edges.add(new ch.Edge(2, node_D));
  //
  //     node_B.edges.add(new ch.Edge(2, node_C));
  //
  //     node_C.edges.add(new ch.Edge(3, node_D));
  //     node_C.edges.add(new ch.Edge(1, node_A));
  //
  //     nodes..add(node_A)..add(node_B)..add(node_C)..add(node_D);
  //     c_h = new ch.ContractionHierarchies.fromListOfNode(nodes);
  //     c_h.preprocessing(saveToPaths: true);
  //   });
  //
  //   test('A -> D', () {
  //     List<ch.Path> paths = c_h.queryingList("A", "D");
  //     expect(paths.toString(), "[A [] D 2]");
  //   });
  //
  //   test('A -> D with queryingOne', () {
  //     ch.Path path = c_h.queryingOne("A", "D");
  //     expect(path.toString(), "A [] D 2");
  //   });
  //
  //   test('A -> B', () {
  //     List<ch.Path> paths = c_h.queryingList("A", "B");
  //     expect(paths.toString(), "[A [] B 1]");
  //   });
  //
  //   test('B -> D', () {
  //     List<ch.Path> paths = c_h.queryingList("B", "D");
  //     expect(paths.toString(), "[B [C] D 5, B [C, A] D 5]");
  //   });
  //
  //   test('B -> D with queryingOne', () {
  //     ch.Path path = c_h.queryingOne("B", "D");
  //     expect(path.toString(), "B [C] D 5");
  //   });
  //
  //   test('C -> D', () {
  //     List<ch.Path> paths = c_h.queryingList("C", "D");
  //     expect(paths.toString(), "[C [] D 3, C [A] D 3]");
  //   });
  //
  //   test('C -> D with queryingOne', () {
  //     ch.Path path = c_h.queryingOne("C", "D");
  //     expect(path.toString(), "C [] D 3");
  //   });
  //
  // });
}
