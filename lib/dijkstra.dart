// Copyright (c) 2016, Kevin Segaud. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

class Arc {
  double weight;
  Node head;
  Node tail;

  Arc(Node this.tail, Node this.head, double this.weight);

  @override
  String toString() => "$weight";
}

class Node {
  double latitude;
  double longitude;

  List<Arc> incomingArcs;
  List<Arc> outcomingArcs;

  Node(double this.latitude, double this.longitude) {
    incomingArcs = <Arc>[];
    outcomingArcs = <Arc>[];
  }

  void addIncomingArc(Arc a) {
    incomingArcs.add(a);
  }

  void addOutcomingArc(Arc a) {
    outcomingArcs.add(a);
  }

  @override
  bool operator ==(Node node) {
    return ((latitude == node.latitude) && (longitude == node.longitude));
  }

  @override
  String toString() => "$latitude $longitude $incomingArcs $outcomingArcs";
}

class DijkstraResult {
  Map<Node, double> dist;
  Map<Node, Node> prev;

  DijkstraResult(this.dist, this.prev);
}

List<double> getArcDurationForEachStepFromDijkstra(
    DijkstraResult result, Node start, Node end) {
  double dist = result.dist[end];
  if (dist == double.INFINITY) {
    return null;
  }
  List<double> list_arc_duration = <double>[];
  list_arc_duration.add(result.dist[end]);
  Node p = result.prev[end];
  while (p != null && p != start) {
    double arc_duration = result.dist[p];
    list_arc_duration.add(arc_duration);
    p = result.prev[p];
  }
  if (p == null) {
    print("p == null");
    return null;
  }
  list_arc_duration = list_arc_duration.reversed;
  double previous_duration = 0.0;
  List<double> durations = <double>[];
  list_arc_duration.forEach((double duration) {
    durations.add(duration - previous_duration);
    previous_duration += duration - previous_duration;
  });
  return durations;
}

Node _findSmallestInMap(Map<Node, double> m) {
  double min = double.INFINITY;
  Node save = m.keys.first;
  m.forEach((Node n, double dist) {
    if (dist < min) {
      min = dist;
      save = n;
    }
  });
  return save;
}

DijkstraResult dijkstraSearch(List<Node> g, Node source, Node end) {
  Map<Node, double> dist = new Map.fromIterable(g,
      key: (item) => item,
      value: (item) => item == source ? 0.0 : double.INFINITY);
  Map<Node, Node> prev =
      new Map.fromIterable(g, key: (item) => item, value: (_) => null);
  Map<Node, double> priority_queue = <Node, double>{};
  g.forEach((Node node) {
    priority_queue[node] = dist[node];
  });
  while (priority_queue.isNotEmpty) {
    Node node = _findSmallestInMap(priority_queue);
    if (node == end) break;
    priority_queue.remove(node);
    node.outcomingArcs.forEach((Arc a) {
      double d_traveled = dist[node] + a.weight;
      if (d_traveled < dist[a.head]) {
        dist[a.head] = d_traveled;
        prev[a.head] = node;
        priority_queue[a.head] = d_traveled;
      }
    });
  }
  return new DijkstraResult(dist, prev);
}
