import "dart:async";
import "package:dijkstra/dijkstra.dart";

Future main() async {
  List<Node> nodes = <Node>[];

  Node node1 = new Node(1.0, 0.0);
  Node node2 = new Node(2.0, 0.0);
  Node node3 = new Node(3.0, 0.0);
  Node node4 = new Node(4.0, 0.0);
  Node node5 = new Node(5.0, 0.0);
  Node node6 = new Node(6.0, 0.0);
  Node node7 = new Node(7.0, 0.0);

  node1.addOutcomingArc(new Arc(node1, node2, 7.0));
  node1.addOutcomingArc(new Arc(node1, node3, 9.0));
  node1.addOutcomingArc(new Arc(node1, node6, 14.0));

  node1.addIncomingArc(new Arc(node2, node1, 7.0));
  node1.addIncomingArc(new Arc(node3, node1, 9.0));
  node1.addIncomingArc(new Arc(node6, node1, 14.0));

  node2.addOutcomingArc(new Arc(node2, node1, 7.0));
  node2.addOutcomingArc(new Arc(node2, node3, 10.0));
  node2.addOutcomingArc(new Arc(node2, node4, 15.0));

  node2.addIncomingArc(new Arc(node1, node2, 7.0));
  node2.addIncomingArc(new Arc(node3, node2, 10.0));
  node2.addIncomingArc(new Arc(node4, node2, 15.0));

  node3.addOutcomingArc(new Arc(node3, node1, 9.0));
  node3.addOutcomingArc(new Arc(node3, node2, 10.0));
  node3.addOutcomingArc(new Arc(node3, node4, 11.0));
  node3.addOutcomingArc(new Arc(node3, node6, 2.0));

  node3.addIncomingArc(new Arc(node1, node3, 9.0));
  node3.addIncomingArc(new Arc(node2, node3, 10.0));
  node3.addIncomingArc(new Arc(node4, node3, 11.0));
  node3.addIncomingArc(new Arc(node6, node3, 2.0));

  node4.addOutcomingArc(new Arc(node4, node2, 15.0));
  node4.addOutcomingArc(new Arc(node4, node3, 11.0));
  node4.addOutcomingArc(new Arc(node4, node5, 6.0));

  node4.addIncomingArc(new Arc(node2, node4, 15.0));
  node4.addIncomingArc(new Arc(node3, node4, 11.0));
  node4.addIncomingArc(new Arc(node5, node4, 6.0));

  node5.addOutcomingArc(new Arc(node5, node4, 6.0));
  node5.addOutcomingArc(new Arc(node5, node6, 9.0));
  node5.addOutcomingArc(new Arc(node5, node7, 2.0));

  node5.addIncomingArc(new Arc(node4, node5, 6.0));
  node5.addIncomingArc(new Arc(node6, node5, 9.0));
  node5.addIncomingArc(new Arc(node7, node5, 2.0));

  node6.addOutcomingArc(new Arc(node6, node1, 14.0));
  node6.addOutcomingArc(new Arc(node6, node3, 2.0));
  node6.addOutcomingArc(new Arc(node6, node5, 9.0));
  node6.addOutcomingArc(new Arc(node6, node7, 12.0));

  node6.addIncomingArc(new Arc(node1, node6, 14.0));
  node6.addIncomingArc(new Arc(node3, node6, 2.0));
  node6.addIncomingArc(new Arc(node5, node6, 9.0));
  node6.addIncomingArc(new Arc(node7, node6, 12.0));

  node7.addOutcomingArc(new Arc(node7, node5, 2.0));
  node7.addOutcomingArc(new Arc(node7, node6, 12.0));

  node7.addIncomingArc(new Arc(node5, node7, 2.0));
  node7.addIncomingArc(new Arc(node6, node7, 12.0));

  nodes
    ..add(node1)
    ..add(node2)
    ..add(node3)
    ..add(node4)
    ..add(node5)
    ..add(node6)
    ..add(node7);

  Node start = node1;
  Node end = node4;
  // DijkstraResult result = d.bidirectionalSearch(nodes, start, end);
  DijkstraResult result = dijkstraSearch(nodes, start, end);
  if (result == null) {
    print("NO PATH");
    return 0;
  }
  List<double> durations =
      getArcDurationForEachStepFromDijkstra(result, start, end);
  if (durations == null) {
    print("NO PATH");
    return 0;
  }
  StringBuffer sb = new StringBuffer();
  for (int i = 0; i < durations.length; ++i) {
    double duration = durations[i];
    sb.write(duration);
    if (i != durations.length - 1) sb.write(" ");
  }
  print("my result        : ${result.dist[end]} ${sb.toString()}");
  // print("path             : ${sb.toString()}");
}
